# Minimal react boilerplate

This includes react, styled-components, eslint, prettier and parcel. Parcel is used as an application bundler.

## Scripts

### Dev

`npm start`

Website can be found running on http://localhost:3000

### Build

`npm run build`

Output of build will be found in the dist folder
