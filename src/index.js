import React from 'react';
import ReactDDOM from 'react-dom';

import App from './App';

ReactDDOM.render(
	<App />,
	document.getElementById('root')
);
