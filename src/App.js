import React from 'react';

import StyledApp, { BoxWrapper, Buttons } from './App.styles';
import Box from './components/Box/Box';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			boxes: this.boxes,
		};
		this.filter = this.filter.bind(this);
	}

	get boxes() {
		return ['A', 'C', 'B', 'E', 'B', 'D', 'A', 'B', 'C', 'D', 'A', 'E'];
	}

	filter(letter) {
		return () => {
			const boxes = !letter
				? this.boxes
				: this.boxes.filter(box => box === letter);
			this.setState({
				boxes,
			});
		};
	}

	render() {
		const { boxes } = this.state;
		return (
			<StyledApp>
				<Buttons>
					<button onClick={this.filter()}>All</button>
					<button onClick={this.filter('A')}>A</button>
					<button onClick={this.filter('B')}>B</button>
					<button onClick={this.filter('C')}>C</button>
					<button onClick={this.filter('D')}>D</button>
					<button onClick={this.filter('E')}>E</button>
				</Buttons>
				<BoxWrapper>
					{boxes.map((box, i) => (
						<Box key={i} large={i === 0 || i === 7}>
							{box}
						</Box>
					))}
				</BoxWrapper>
			</StyledApp>
		);
	}
}

App.description = `
	
`;

export default App;
