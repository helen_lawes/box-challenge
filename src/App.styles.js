import styled, { injectGlobal } from 'styled-components';
import { em, rem, media } from './utils/styles';

injectGlobal`
	* {
		box-sizing: border-box;
	}
	body {
		font: 1em/normal sans-serif;
		margin: 0;
		padding: 0;
	}
`;

const StyledApp = styled.div`
	max-width: ${rem(940)};
	overflow: hidden;
	margin: 0 auto;
`;

export default StyledApp;

export const BoxWrapper = styled.div`
	padding: 5vw;
	${media.medium`	
		display: grid;
		grid-template-columns: auto auto auto;
		grid-gap: ${rem(20)};
		padding: 0;
	`};
`;

export const Buttons = styled.div`
	display: flex;
	justify-content: space-between;
	margin: ${rem(20)} 0;
	padding: 0 5vw;
	overflow-y: auto;
	${media.medium`
		padding: 0;
	`};
	button {
		min-width: ${rem(120)};
		padding: ${em(5)} ${rem(10)};
	}
`;
