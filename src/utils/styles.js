import { css } from 'styled-components';

const colors = {
	white: '#fff',
	grey: '#333',
};

export const getColor = color => colors[color];

const sizes = {
	medium: 940,
	small: 430,
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
	acc[label] = (...args) => css`
		@media (min-width: ${sizes[label] / 16}em) {
			${css(...args)};
		}
	`;

	return acc;
}, {});

export const em = (px, basePx = 16) => `${px / basePx}em`;
export const rem = (px, basePx = 16) => `${px / basePx}rem`;
