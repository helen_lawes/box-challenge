import styled from 'styled-components';
import { getColor, rem, media } from '../../utils/styles';

export const margin = 20;
export const boxSize = 300;

const StyledBox = styled.div`
	width: 90vw;
	height: ${props => (props.large ? '90vw' : '45vw')};
	background: ${getColor('grey')};
	color: ${getColor('white')};
	font-size: 2em;
	margin-bottom: ${rem(margin)};
	display: grid;
	align-content: center;
	justify-content: center;
	${media.medium`
		width: ${props => (props.large ? rem(boxSize * 2 + margin) : rem(boxSize))};
		height: ${props => (props.large ? rem(boxSize * 2 + margin) : rem(boxSize))};
		margin-bottom: 0;
	`};
	${props =>
		props.large &&
		media.medium`
			grid-column: span 2;
			grid-row: span 2;
		`};
`;

export default StyledBox;
