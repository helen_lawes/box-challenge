import PropTypes from 'prop-types';

import StyledBox from './Box.styles';

const Box = StyledBox;

Box.displayName = 'Box';

Box.description = `
	Box component
`;

Box.propTypes = {
	/** content to display in box */
	children: PropTypes.node,
	/** large box variant */
	large: PropTypes.bool,
};

export default Box;
